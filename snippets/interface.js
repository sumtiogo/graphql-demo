const { gql, ApolloServer } = require("apollo-server");

const typeDefs = gql`
  interface Animal {
    name: String
  }

  type Bird implements Animal {
    name: String
    "翅膀展開長度"
    wingSpanLength: Int
  }

  type Monkey implements Animal {
    name: String
    "手臂展開長度"
    armSpanLength: Int
  }

  type Query {
    animal(name: String): Animal
    animals: [Animal]
  }
`;

const animals = [
  { name: "Chiken Litte", wingSpanLength: 10 },
  { name: "Goku", armSpanLength: 20 },
  { name: "King Kong", armSpanLength: 200 },
];

const resolvers = {
  Animal: {
    // 一定要實作這一個特殊 field
    __resolveType(obj, context, info) {
      // obj 為該 field 得到的資料
      if (obj.wingSpanLength) {
        // 回傳相對應得 Object type 名稱
        return "Bird";
      }

      if (obj.armSpanLength) {
        return "Monkey";
      }

      return null;
    },
  },
  Query: {
    animal: (root, { name }) => animals.find((animal) => animal.name === name),
    animals: () => animals,
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});

// query
// {
//   animals {
//     name
//     ... on Bird {
//       wingSpanLength
//     }
//     ... on Monkey {
//       armSpanLength
//     }
//   }
// }
