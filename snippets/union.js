const { gql, ApolloServer } = require("apollo-server");

const typeDefs = gql`
  union Result = Book | Author

  type Book {
    title: String
  }

  type Author {
    name: String
  }

  type Query {
    search(contains: String!): [Result]
  }
`;
const authors = [{ name: "John" }, { name: "Mary" }];
const books = [{ title: "Journey to the West" }, { title: "Mary Loves Me" }];

const resolvers = {
  Result: {
    // 一定要實作這一個特殊 field
    __resolveType(obj, context, info) {
      // obj 為該 field 得到的資料
      if (obj.name) {
        // 回傳相對應得 Object type 名稱
        return "Author";
      }

      if (obj.title) {
        return "Book";
      }

      return null;
    },
  },
  Query: {
    search: (root, { contains }) => [
      ...authors.filter((author) => author.name.includes(contains)),
      ...books.filter((book) => book.title.includes(contains)),
    ],
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});

// query
// {
//   animals {
//     name
//     ... on Bird {
//       wingSpanLength
//     }
//     ... on Monkey {
//       armSpanLength
//     }
//   }
// }
