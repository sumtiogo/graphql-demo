const {
  gql,
  SchemaDirectiveVisitor,
  ForbiddenError,
} = require("apollo-server");

const userSchema = require("./user");
const postSchema = require("./post");

// Construct a schema, using GraphQL schema language
const typeDefs = gql`
  directive @upper on FIELD_DEFINITION
  directive @isAuthenticated on FIELD_DEFINITION

  type Query {
    "測試用 Hello World"
    hello: String @upper
  }

  type Mutation {
    test: String
  }
`;

class IsAuthenticatedDirective extends SchemaDirectiveVisitor {
  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;
    field.resolve = async function (...args) {
      const context = args[2];
      // 檢查有沒有 context.me
      if (!context.me) throw new ForbiddenError("Directive: Not logged in.");

      // 確定有 context.me 後才進入 Resolve Function
      const result = await resolve.apply(this, args);
      return result;
    };
  }
}

class UpperCaseDirective extends SchemaDirectiveVisitor {
  // 2-1. ovveride field Definition 的實作
  visitFieldDefinition(field) {
    const { resolve = defaultFieldResolver } = field;
    // 2-2. 更改 field 的 resolve function
    field.resolve = async function (...args) {
      // 2-3. 取得原先 field resolver 的計算結果 (因為 field resolver 傳回來的有可能是 promise 故使用 await)
      const result = await resolve.apply(this, args);
      // 2-4. 將得到的結果再做預期的計算 (toUpperCase)
      if (typeof result === "string") {
        return result.toUpperCase();
      }
      // 2-5. 回傳最終值 (給前端)
      return result;
    };
  }
}

// Resolvers
const resolvers = {
  Query: {
    hello: () => "world",
  },
  Mutation: {
    test: () => "test",
  },
};

module.exports = {
  typeDefs: [typeDefs, userSchema.typeDefs, postSchema.typeDefs],
  resolvers: [resolvers, userSchema.resolvers, postSchema.resolvers],
  schemaDirectives: {
    isAuthenticated: IsAuthenticatedDirective,
    upper: UpperCaseDirective,
  },
};
