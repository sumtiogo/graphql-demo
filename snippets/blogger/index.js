const { ApolloServer } = require("apollo-server");
const jwt = require("jsonwebtoken");

const { typeDefs, resolvers, schemaDirectives } = require("./schema");
const { userModel, postModel } = require("./models");

require("dotenv").config();

// 定義 bcrypt 加密所需 saltRounds 次數
const SALT_ROUNDS = Number(process.env.SALT_ROUNDS);
// 定義 jwt 所需 secret (可隨便打)
const SECRET = process.env.SECRET;

const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: async ({ req }) => {
    const context = {
      secret: SECRET,
      saltRounds: SALT_ROUNDS,
      userModel,
      postModel,
    };
    // 1. 取出 token
    const token = req.headers["x-token"];
    if (token) {
      try {
        // 2. 檢查 token + 取得解析出的資料
        const me = await jwt.verify(token, SECRET);
        // 3. 放進 context
        return { ...context, me };
      } catch (e) {
        throw new Error("Your session expired. Sign in again.");
      }
    }
    // 如果沒有 token 就回傳空的 context 出去
    return context;
  },
  schemaDirectives,
});

server.listen().then(({ url }) => {
  console.log(`🚀 Server ready at ${url}`);
});
