const { ApolloServer, gql, MockList } = require("apollo-server");

const typeDefs = gql`
  type Person {
    name: String
    age: Int
  }
  type Query {
    hello: String
    getString: String
    getInt: Int
    getFloat: Float
    people(first: Int): [Person]
  }
`;

const resolvers = {
  Query: {
    hello: () => "Not used",
  },
};

const mocks = {
  Int: () => 6,
  Float: () => 22.1,
  String: () => "Hello",
  Query: () => ({
    people: (root, { first }, context) => {
      if (first) {
        return new MockList([0, first]);
      }
      return new MockList([0, 12]);
    },
  }),
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  mocks,
});

server.listen().then(({ url }) => {
  console.log(`? Server ready at ${url}`);
});
