// this file is an unexecutable pseudo code
const { gql, UserInputError } = require("apollo-server");

const typeDefs = gql`
  type PostConnection {
    "資料"
    edges: [PostEdge!]!
    "分頁資訊"
    pageInfo: PageInfo!
  }

  type PostEdge {
    "指標。通常為一串 base64 字元"
    cursor: String!
    "實際 Post 資料"
    node: Post!
  }

  type PageInfo {
    "是否有下一頁"
    hasNextPage: Boolean!
    "是否有上一頁"
    hasPreviousPage: Boolean!
    "總頁數"
    totalPageCount: Int
  }
  type Query {
    posts(
      "回傳開頭的前 N 筆資料"
      first: Int
      "會回傳該 curosr 後面的資料。一定要搭配 first"
      after: String
      "回傳倒數的 N 筆資料。一定要搭配 before"
      last: Int
      "會回傳該 curosr 前面的資料。一定要搭配 last"
      before: String
    ): PostConnection!
  }
`;

const resolvers = {
  Query: {
    posts: async (root, { first, after, last, before }, { db }) => {
      if (!first && after) throw new UserInputError("after must be with first");
      if ((last && !before) || (!last && before))
        throw new UserInputError("last and before must be used together");
      if (first && after && last && before)
        throw new UserInputError("Incorrect Arguments Usage.");

      let posts;
      // 取得下一頁資料
      if (first) {
        posts = after
          ? await db.query(
              "SELECT *, count(*) OVER() AS count FROM post WHERE created_at < $1 ORDER BY created_at DESC LIMIT $2",
              [Buffer.from(after, "base64").toString(), first]
            )
          : await db.query(
              "SELECT * FROM post ORDER BY created_at DESC LIMIT $1",
              [first]
            );
      }

      // 或是取得上一頁資料
      if (last) {
        posts = await db.query(
          `SELECT * FROM (
              SELECT *, count(*) OVER() AS count FROM post WHERE created_at > $1 ORDER BY created_at ASC LIMIT $2
           ) posts ORDER BY created_at DESC`,
          [Buffer.from(before, "base64").toString(), last]
        );
      }

      // 取得有條件 (WHERE) 但未限制數量 (LIMIT) 的真正數量
      const countWithoutLimit = posts[0].count;
      // 取得總數量
      const allCount = db.query("SELECT count(*) as number FROM post;").count;

      return {
        edges: posts.map((post) => ({
          // 指標 (將 createdAt 做 base64)
          cursor: Buffer.from(post.createdAt).toString("base64"),
          // 實際資料
          node: post,
        })),
        pageInfo: {
          // 檢查有無下一頁
          hasNextPage: first
            ? countWithoutLimit > first
            : allCount > countWithoutLimit,
          // 檢查有無上一頁
          hasPreviousPage: last
            ? countWithoutLimit > last
            : alCount > countWithoutLimit,
          // 總頁數
          totalPageCount: Math.ceil(allCount / (fist || last)),
        },
      };
    },
  },
};
